module bitbucket.org/durdn/goguru

require (
	github.com/go-chi/chi v3.3.3+incompatible // indirect
	github.com/go-kit/kit v0.7.0
	github.com/go-logfmt/logfmt v0.3.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/gorilla/handlers v1.4.0
	github.com/gorilla/websocket v1.4.0
	github.com/pressly/chi v3.3.3+incompatible
	golang.org/x/exp v0.0.0-20190125153040-c74c464bbbf2
	gonum.org/v1/gonum v0.0.0-20190221132855-8ea67971a689
)

replace bitbucket.org/durdn/goguru => ../goguru
