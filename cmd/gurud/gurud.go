package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime/pprof"
	"strings"

	"bitbucket.org/durdn/goguru/pkg/daemon"
	"github.com/go-kit/kit/log"
	"github.com/gorilla/handlers"
	"github.com/pressly/chi"
	"github.com/pressly/chi/middleware"
)

var tlsConfig = &tls.Config{
	MinVersion:               tls.VersionTLS12,
	CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
	PreferServerCipherSuites: true,
	CipherSuites: []uint16{
		tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
		tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
		tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
		tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
		tls.TLS_RSA_WITH_AES_256_CBC_SHA,
	},
}

func server(address string, router *chi.Mux) *http.Server {
	return &http.Server{
		// Addr:         address,
		// TLSConfig:    tlsConfig,
		// TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
		Handler: handlers.CORS(
			handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}),
			handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"}),
			handlers.AllowedOrigins([]string{"*"}))(router),
	}
}

// fileServerWithCacheHeaders conveniently sets up a http.FileServer handler to serve
// static files from a http.FileSystem with extra CacheHeaders set
func fileServerWithCacheHeaders(app *daemon.App, r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, ":*") {
		panic("chi: FileServer does not permit URL parameters.")
	}

	fs := http.StripPrefix(path, http.FileServer(root))

	path += "*"

	r.Get("/ws", http.HandlerFunc(app.Websocket))
	r.Get(path, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Ask to refresh cache in 3 months
		w.Header().Set("Cache-Control", "public, max-age=7776000")
		fs.ServeHTTP(w, r)
	}))
}

// handleRequests setup Routes for entire Application
func handleRequests(app *daemon.App) {
	r := chi.NewRouter()

	fileServerWithCacheHeaders(app, r.With(middleware.DefaultCompress), "/", http.Dir(app.AssetsPath))
	app.Logger.Log("addr", app.Addr, "msg", "server listening")
	err := server(app.Addr, r).ListenAndServe()
	if err != nil {
		fmt.Print(err)
		app.Logger.Log("err", err)
	}
}

func main() {
	var logger log.Logger
	var addr = flag.String("addr", "0.0.0.0:80", "backend ip address and port")
	var assets = flag.String("assets", "./web/dist", "path to static web assets")
	var cert = flag.String("cert", "dev.cert", "path to ssl certificate")
	var key = flag.String("key", "dev.key", "path to ssl key")
	var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

	flag.Parse()

	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			logger.Log(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.With(logger, "ts", log.DefaultTimestampUTC)
		logger = log.With(logger, "caller", log.DefaultCaller)
	}

	app := &daemon.App{
		Logger:      logger,
		Addr:        *addr,
		Clients:     make(map[string]*daemon.Client),
		AssetsPath:  *assets,
		SSLCertPath: *cert,
		SSLKeyPath:  *key,
	}

	// Capture SIGINT aka ctrl-c
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			pprof.StopCPUProfile()
			os.Exit(0)
		}
	}()

	handleRequests(app)
}
