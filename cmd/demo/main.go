package main

import (
	"flag"
	"fmt"
	"os"

	"bitbucket.org/durdn/goguru/pkg/sim"
	"github.com/go-kit/kit/log"

	// "net/http/pprof"
	"runtime/pprof"
)

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func main() {
	logger := log.NewLogfmtLogger(os.Stdout)
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			logger.Log(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	logger = log.With(logger, "caller", log.DefaultCaller)

	// Initial Random Distribution for Nodes can be: normal, exponential or none
	// Committee selection distribution can be: exponential, triangular or normal
	s := sim.NewSimulation(5000, 100, "normal", 0.4, "triangular", logger)
	s.Logger.Log("msg", "Nodes ordered by initial reputation", "repus", fmt.Sprintf("%+v", s.Reputations()[:5]))
	s.Logger.Log("msg", "Malicious nodes", "malicous", s.MaliciousCount(), "total", len(s.Nodes))
	s.RunCycle(100)
	s.Log()
	s.Logger.Log("msg", "Add botnet of 800 malicious nodes")

	// Add 800 malicious nodes
	s.BotnetTakeover(-800)
	for i := 0; i < 10; i++ {
		s.RunCycle(100)
		s.Log()
	}
}
