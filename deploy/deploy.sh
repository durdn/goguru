#!/bin/bash
# Expects environment variables $HOST (i.e. user@some.host) and $DEST (i.e. /some/path)
# Expects TLS/SSL certificates in the root folder named prod.cert and prod.key
rm -f goguru.tar.gz
git archive --output=goguru.tar.gz HEAD
scp goguru.tar.gz $HOST:$DEST
rm -f goguru.tar.gz
ssh $HOST tar zxf $DEST/goguru.tar.gz -C $DEST
ssh $HOST chmod 755 $DEST/deploy/run.sh
ssh $HOST $DEST/deploy/run.sh
