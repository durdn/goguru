#!/bin/bash -x
cd /tmp/goguru
sudo docker-compose down
sudo docker build --rm=true --tag=durdn/gurud -f gurud.static.dockerfile --build-arg environment=prod .
sudo docker-compose up -d
# rm -rf /tmp/finplan