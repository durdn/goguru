FROM alpine

EXPOSE 80

ARG environment=dev

ADD gurud /
COPY ui.tar.gz .
RUN tar zxf ui.tar.gz
RUN rm ui.tar.gz
ENV stage $environment
CMD /gurud -assets=./dist