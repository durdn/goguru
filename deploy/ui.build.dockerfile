FROM node:alpine

WORKDIR /web

RUN apk add yarn
RUN yarn global add @vue/cli &> /dev/null
RUN yarn global add @vue/cli-service-global &> /dev/null

COPY web /web

RUN yarn --silent
RUN vue build
RUN tar zcvf ui.tar.gz ./dist/

CMD ["/bin/sh"]