FROM golang:alpine

# ENV GO111MODULE on
WORKDIR /go/src/bitbucket.org/durdn/goguru

RUN apk add git

COPY pkg ./pkg
COPY cmd ./cmd
COPY go.* ./

RUN go get -d ./...
RUN go build -ldflags "-s" ./...
RUN go install ./...

CMD ["/bin/sh"]