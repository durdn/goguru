import { Line, mixins } from 'vue-chartjs';
const { reactiveProp } = mixins;

export default {
  extends: Line,
  mixins: [reactiveProp],
  // props: ['options'],
  mounted() {
    this.options = {
      // elements: {
      //   line: {
      //     tension: 0 // disables bezier curves
      //   }
      // },
      // showLines: false,
      tooltips: {
        enabled: false
      },
      scales: {
        xAxes: [
          {
            ticks: {
              display: false
            },
            gridLines: {
              display: false
            }
          }
        ]
      }
    };
    this.renderChart(this.chartData, this.options);
  }
};
