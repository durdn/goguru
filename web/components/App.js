import ReputationChart from './ReputationChart';
import SuccessRatesChart from './SuccessRatesChart';
import MaliciousChart from './MaliciousChart';
import { throttle, range } from 'lodash';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import * as $ from 'jquery';
import bootstrap from 'bootstrap';
window.$ = $;
window.jQuery = $;
window.bootstrap = bootstrap;

export default {
  components: {
    ReputationChart,
    SuccessRatesChart,
    MaliciousChart
  },
  data() {
    return {
      reputations: {
        labels: [],
        datasets: [
          {
            label: 'Nodes ordered by reputations',
            backgroundColor: '#28A745'
          }
        ]
      },
      successRates: {
        labels: [],
        datasets: [
          {
            xAxisID: 'x',
            label: 'Successes over 100 rounds',
            borderColor: '#0069d9'
          }
        ]
      },
      maliciousRateBuckets: {
        labels: [],
        datasets: [
          {
            label: 'Malicious rate in 10 buckets',
            backgroundColor: 'orange'
          }
        ]
      },
      msg: 'idle',
      status: 'idle',
      ws: '',
      code: 'light',
      nrNodes: 0,
      maliciousCount: 0,
      retries: 0,
      retryTimerObj: null,
      roundNumber: 0
    };
  },
  mounted() {
    this.draw = throttle(this.draw, 250, { trailing: false });
    this.setupWebsocket();
  },
  computed: {
    isDisabled() {
      return (
        this.status !== 'connected' &&
        this.status !== 'running' &&
        this.status !== 'stopped'
      );
    }
  },
  methods: {
    retryTimer() {
      var outer = this;
      if (!this.retryTimerObj) {
        this.retryTimerObj = setInterval(() => {
          outer.retries++;
          if (outer.retries >= 5) {
            outer.msg = 'refresh to retry';
            outer.status = 'idle';
            outer.code = 'light';
            outer.ws = null;
            outer.retries = 0;
            clearInterval(outer.retryTimerObj);
            outer.retryTimerObj = null;
            return;
          }
          outer.msg =
            'websocket: reconnect attempt (' + outer.retries + ') in 5s';
          outer.ws = null;
          outer.status = 'retry ' + outer.retries;
          outer.code = 'warning';
          outer.setupWebsocket();
        }, 3000);
      }
    },
    setupWebsocket() {
      this.ws = new WebSocket('wss://' + window.location.hostname + '/ws');
      this.ws.onopen = () => {
        this.msg = 'websocket connected';
        if (this.status !== 'reset') {
          this.status = 'connected';
        }
        this.code = 'success';
        clearInterval(this.retryTimerObj);
        this.retryTimerObj = null;
        this.retries = 0;
      };
      this.ws.onmessage = e => {
        var res = JSON.parse(e.data);
        // if (res.Command !== 'stats') {
        //   console.log('received:' + res.Command);
        // }
        if (
          res.Command === 'stats' &&
          res.Success &&
          this.status !== 'stopped'
        ) {
          this.msg = 'Simulation running: Receiving cycle updates...';
          this.status = 'running';
          this.code = 'success';
          this.draw(res);
        }
        if (res.Command === 'stop' && res.Success) {
          this.status = 'stopped';
          this.code = 'danger';
          this.draw(res);
        }
        if (res.Command === 'start' && res.Success) {
          this.status = 'running';
          this.code = 'success';
        }
        if (res.Command === 'reset' && res.Success) {
          this.status = 'reset';
          this.code = 'success';
          this.ws.close();
        }
      };

      this.ws.onclose = () => {
        if (this.status === 'quitting') {
          this.status = 'idle';
          this.code = 'light';
          clearInterval(this.retryTimerObj);
          this.retryTimerObj = null;
          this.retries = 0;
          return;
        }
        if (this.status !== 'reset') {
          this.msg = 'websocket disconnected';
          this.status = 'disconnected';
          this.code = 'danger';
        }
        if (this.retries == 0) {
          this.retryTimer();
        }
      };

      this.ws.onerror = evt => {
        if (this.status === 'quitting') {
          this.status = 'idle';
          this.code = 'light';
          return;
        }
        this.msg = 'websocket error:' + evt.data;
        this.status = 'error';
      };
    },
    start() {
      if (this.isDisabled) {
        return;
      }
      if (
        this.status === 'connected' ||
        this.status === 'reset' ||
        this.status === 'stopped'
      ) {
        this.ws.send(JSON.stringify({ Command: 'start' }));
        this.msg = 'Simulation started: Generating nodes...';
        this.status = 'starting';
        this.code = 'warning';
      }
    },
    quit() {
      this.status = 'quitting';
      this.ws.send(JSON.stringify({ Command: 'quit' }));
      this.msg = 'Shutting backend down';
    },
    reset() {
      this.ws.send(JSON.stringify({ Command: 'reset' }));
      this.msg = 'Simulation is being reset...';
      this.status = 'resetting';
      this.code = 'warning';
    },
    stop() {
      if (this.isDisabled) {
        return;
      }
      this.ws.send(JSON.stringify({ Command: 'stop' }));
      this.msg = 'Simulation stopping...';
      this.status = 'stopping';
      this.code = 'warning';
    },
    takeover(nrBots) {
      this.ws.send(JSON.stringify({ Command: 'takeover', BotsNumber: nrBots }));
    },
    sybil(nrSybils) {
      this.ws.send(JSON.stringify({ Command: 'sybil', SybilNumber: nrSybils }));
    },
    draw(data) {
      if (this.status === 'stopped') {
        return;
      }
      if (this.status === 'quitting') {
        return;
      }
      this.maliciousCount = data.Stats.MaliciousCount;
      this.nrNodes = data.Stats.NrNodes;
      this.roundNumber = data.Stats.RoundNumber;
      this.reputations = {
        labels: range(data.Stats.Reputations.length),
        datasets: [
          {
            label: 'Nodes ordered by reputations',
            borderColor: '#28A745',
            data: data.Stats.Reputations,
            pointRadius: 1
          }
        ]
      };
      if (data.Stats.CycleSuccessRates.length > 0) {
        var csrMean = [];
        for (var i = 2; i < data.Stats.CycleSuccessRates.length - 2; i++) {
          var mean =
            (data.Stats.CycleSuccessRates[i] +
              data.Stats.CycleSuccessRates[i - 2] +
              data.Stats.CycleSuccessRates[i - 1] +
              data.Stats.CycleSuccessRates[i + 1] +
              data.Stats.CycleSuccessRates[i + 2]) /
            5.0;
          csrMean.push(mean);
        }
        this.successRates = {
          labels: csrMean,
          datasets: [
            {
              data: csrMean,
              datasets: [{ xAxisID: 'x' }],
              label: 'Successes over 100 rounds',
              borderColor: '#0069d9',
              borderWidth: 1,
              pointRadius: 0,
              fill: false
            }
          ]
        };
      }
      this.maliciousRateBuckets = {
        labels: range(10),
        datasets: [
          {
            label: 'Malicious rate in 10 buckets',
            backgroundColor: 'orange',
            data: data.Stats.MaliciousNodeRateBuckets
          }
        ]
      };
    }
  }
};
