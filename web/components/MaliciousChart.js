import { Bar, mixins } from 'vue-chartjs';
const { reactiveProp } = mixins;

export default {
  extends: Bar,
  mixins: [reactiveProp],
  // props: ['options'],
  mounted() {
    this.options = {
      tooltips: {
        enabled: false
      },
      hover: {
        animationDuration: 0 // duration of animations when hovering an item
      }
    };
    this.renderChart(this.chartData, this.options);
  }
};
