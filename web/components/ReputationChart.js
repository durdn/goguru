import { Line, mixins } from 'vue-chartjs';
const { reactiveProp } = mixins;

export default {
  extends: Line,
  mixins: [reactiveProp],
  // props: ['options'],
  mounted() {
    this.options = {
      // animation: {
      //   duration: 0 // general animation time
      // },
      tooltips: {
        enabled: false
      },
      hover: {
        animationDuration: 0 // duration of animations when hovering an item
      },
      showLines: false,
      fill: false,
      elements: {
        line: {
          tension: 0 // disables bezier curves
        }
      }
    };
    this.renderChart(this.chartData, this.options);
  }
};
