SHELL := /bin/bash
default: buildgurud

buildgurud: buildui
	docker build -t durdn/build-guru-d -f ./deploy/gurud.build.dockerfile .
	docker run -t durdn/build-guru-d /bin/true
	docker cp `docker ps -q -n=1`:/go/bin/gurud .
	docker build --rm=true --tag=durdn/gurud -f ./deploy/gurud.static.dockerfile --build-arg environment=prod .
	rm ./gurud
	rm ./ui.tar.gz

buildui:
	docker build -t durdn/build-guru-ui -f ./deploy/ui.build.dockerfile .
	docker run -t durdn/build-guru-ui /bin/true
	docker cp `docker ps -q -n=1`:/web/ui.tar.gz .

run: buildgurud buildui
#	docker run -e VIRTUAL_PROTO=https -e VIRTUAL_PORT=443 -e VIRTUAL_HOST=guru.paolucci.ventures -d --expose 443 durdn/gurud
	echo building complete

clean:
	docker stop `docker ps -q`
	docker rm `docker ps -aq`
	docker rmi durdn/gurud
