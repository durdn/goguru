# Go implementation of Guru

[Guru](https://docs.google.com/viewer?url=https://eprint.iacr.org/2017/671.pdf)
is a Universal Reputation Module for Distributed Consensus Protocols
developed at the [University of Luxembourg](https://www.cryptolux.org/) by
Alex Biryukov, Daniel Feher, and Dmitry Khovratovich. The original Python
simulation code is on [Github](https://github.com/cryptolu/Guru).

## Pre-requisites

Please make sure you have a development environment with `yarn` and `go` installed and configured.

## Quickstart: run web UI to see the system running live

- Collect Javascript dependencies and compile the web assets:

        cd web
        yarn
        vue build

- Build and start the application - which will run on port `443` and use development certificates for `localhost`

        cd ..
        go build ./cmd/gurud
        ./gurud

- Browse to https://localhost
- Verify that the backend information says 'connected'
- Click on 'Start Simulation'
- Fiddle with adding and removing malicious bots

(Alternatively run "vue build" and deploy the content of the "dist" folder to a static web server)

## Run unit tests

To run unit tests, run:

    go run test ./...

## Dependencies

The application uses the new Golang module system (`1.11` onwards) so it should
grab the required libraries automatically. Check out `go.mod` for a snapshot
of the dependencies.

## Profiling

To create a cpu profile of the running application, run:

    ./gurud -cpuprofile=gurud.prof
    go tool pprof gurud.exe gurud.prof

## Build all in Docker

- Build websocket backend and ui in Docker:

        make

- Run application in containers:

        make run

- Cleanup stale containers:

        docker stop $(docker ps -q)
        docker rm $(docker ps -aq)

- Remove all `<none>` images:

        docker rmi $(docker images | grep none | col 3)

## Provision (Linux/MacOS)

To setup a new environment from scratch:

- Setup a host with Docker
- Set environment variables $HOST (i.e. user@host.com) and $DEST (i.e. /some/path)
- Run:

        ./deploy/deploy.sh

### Generate HTTPS Certificates to deploy on custom domain

- Generate Letsencrypt certificates in Windows 10 / Ubuntu System for Windows 10

        sudo wget https://dl.eff.org/certbot-auto
        ./certbot-auto
        ./certbot-auto --manual certonly --preferred-challenges dns

- Optionally check that the DNS challenge has propagated with this [tool](https://mxtoolbox.com/SuperTool.aspx)

- Certificates will appear in:

        /etc/letsencrypt
