package sim

import (
	"math"

	"bitbucket.org/durdn/goguru/pkg/core"
	"golang.org/x/exp/rand"
)

// BotnetTakeover simulate a bot net taking over the system:
//   negative int to add malicious bots, positive to remove them
func (s *Simulation) BotnetTakeover(botsNumber int) {
	nodeIndexes := []int{}
	btnd := math.Abs(float64(botsNumber))
	if int(botsNumber) < 0 {
		for i := 0; i < s.M; i++ {
			if !s.Nodes[i].Malicious {
				nodeIndexes = append(nodeIndexes, i)
			}
		}
		if botsNumber >= len(nodeIndexes) {
			for i := 0; i < len(nodeIndexes); i++ {
				s.Nodes[nodeIndexes[i]].Malicious = true
			}
			return
		}
		for {
			if 0 < btnd && 0 < len(nodeIndexes)-1 {
				bn := rand.Intn(len(nodeIndexes) - 1)
				s.Nodes[nodeIndexes[bn]].Malicious = true
				btnd--
				nodeIndexes = nodeIndexes[1:]
			} else {
				break
			}
		}
	} else {
		for i := 0; i < s.M; i++ {
			if s.Nodes[i].Malicious {
				nodeIndexes = append(nodeIndexes, i)
			}
		}
		if botsNumber >= len(nodeIndexes) {
			for i := 0; i < len(nodeIndexes); i++ {
				s.Nodes[nodeIndexes[i]].Malicious = false
			}
			return
		}
		for {
			if 0 < btnd && 0 < len(nodeIndexes)-1 {
				bn := rand.Intn(len(nodeIndexes) - 1)
				s.Nodes[nodeIndexes[bn]].Malicious = false
				btnd--
				nodeIndexes = nodeIndexes[1:]
			} else {
				break
			}
		}
	}
}

// SybilJoin simulate a sybil attack:
//   negative int to add malicious bots, positive to remove them
func (s *Simulation) SybilJoin(sybilNodesNumber int) {
	malicious := true
	if sybilNodesNumber > 0 {
		malicious = false
	}
	for i := 0; i < int(math.Abs(float64(sybilNodesNumber))); i++ {
		s.Nodes = append(s.Nodes, core.NewNode(0, malicious))
	}
	s.M = len(s.Nodes)
}
