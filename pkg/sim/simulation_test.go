package sim

import (
	"os"
	"testing"

	"github.com/go-kit/kit/log"
)

func TestSimulationSetup(t *testing.T) {
	logger := log.NewLogfmtLogger(os.Stdout)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	logger = log.With(logger, "caller", log.DefaultCaller)
	s := NewSimulation(20, 10, "normal", 0.4, "triangular", logger)
	s.RunCycle(5)
}
