package sim

import (
	"fmt"
	"sort"
	"time"

	"bitbucket.org/durdn/goguru/pkg/core"
	"bitbucket.org/durdn/goguru/pkg/stats"
	"github.com/go-kit/kit/log"
	"golang.org/x/exp/rand"
)

// Simulation contains the data related to the overall simulation
type Simulation struct {
	Logger    log.Logger
	RequestC  chan Request
	ResponseC chan Response
	Running   bool // false
	Nodes     []core.Node

	// Total number of nodes
	M int // 5000
	// Committee Size
	N int // 100
	// External or Initial Reputation Distribution (normal, exponential, uniform, none)
	ExternalDistribution string
	// Committee Selection Rule (filter, triangular, exponential)
	CommitteeSelectionRule string
	// Malicious rate
	Alpha0 float64 // default = 0.4
	// Counter for the round number
	RoundNumber int
	CycleStep   int // 1
	// CycleCounter how many cycles did happen in this simulation
	CycleCounter int //
	// RoundsInCycle a cycle consists of RoundsInCycle rounds
	RoundsInCycle int
	// Success count
	Success int // 0
	// CycleSuccesses nr of successes in cycle
	CycleSuccesses        int   // 0
	BadBlocks             int   // 0
	CycleSuccessRates     []int // [0]
	TrustedFaultyNodeRate []float64
	// OGProportions         []int
	OverallBadNodes     int
	ProportionalBuckets int // 10
	ToTrack             bool
	OriginalReputations []float64
	// Rate of malicious nodes per proportional bucket (default size 10)
	MaliciousNodeRateBuckets []float64
	// ConsoleTicks flag to decide if to output sigils to log state of rounds, o . x
	ConsoleTicks bool
}

// NewSimulation initialises a new Simulation
func NewSimulation(totalNodes int, committeeSize int, externalReputationDistribution string, alpha00 float64, committeeSelectionRule string, logger log.Logger) Simulation {
	res := Simulation{
		Logger: logger,
		// CommandC command channel to control goroutine where the Simulation runs
		RequestC:              make(chan Request),
		ResponseC:             make(chan Response),
		Running:               false,
		Nodes:                 []core.Node{},
		RoundNumber:           0,
		CycleStep:             1,
		Success:               0,
		CycleSuccesses:        0,
		BadBlocks:             0,
		CycleCounter:          0,
		CycleSuccessRates:     []int{0},
		RoundsInCycle:         100,
		TrustedFaultyNodeRate: []float64{},
		OverallBadNodes:       0,
		M:                     totalNodes,
		N:                     committeeSize,
		ToTrack:               false,
		ProportionalBuckets:   10,
	}

	res.ExternalDistribution = externalReputationDistribution
	res.CommitteeSelectionRule = committeeSelectionRule
	// Maliciousness rate
	res.Alpha0 = alpha00
	logger.Log("msg", "create initial nodes, initialised using maliciousness rate (alpha0) and external distribution given", "alpha0", alpha00, "distrib", res.ExternalDistribution)
	for i := 0; i < res.M; i++ {
		time.Sleep(1 * time.Millisecond)
		randomInitialReputation := stats.InitialReputation(res.ExternalDistribution)
		malicious := rand.Float64() < ((res.Alpha0-0.05)*(1-randomInitialReputation) + 0.05)
		node := core.NewNode(randomInitialReputation, malicious)
		res.Nodes = append(res.Nodes, node)
	}
	sort.Sort(core.ByReputation(res.Nodes))
	for _, n := range res.Nodes {
		res.OriginalReputations = append(res.OriginalReputations, n.OriginalReputation)
	}
	return res
}

// Loop starts a simulation in a go routine and awaits and processes commands
// returns reset = true if it received a command to reset the whole simulation and restart
func (s *Simulation) Loop() (reset bool) {
	ticker := time.NewTicker(50 * time.Millisecond)
	for {
		select {
		case <-ticker.C:
			if s.Running {
				s.RunCycle(100)
			}
		case c := <-s.RequestC:
			switch c.Command {
			case "start":
				s.Running = true
				s.SendAck("start", false)
				continue
			case "stop":
				s.Running = false
				s.SendAck("stop", false)
				continue
			case "reset":
				s.Running = false
				reset = true
				s.SendAck("reset", false)
				return
			case "quit":
				reset = false
				s.SendAck("reset", false)
			case "takeover":
				s.BotnetTakeover(c.BotsNumber)
				s.SendAck("takeover", false)
			case "sybil":
				s.SybilJoin(c.SybilNumber)
				s.SendAck("sybil", false)
				continue
			default:
				panic("command not understood")
			}
		}
	}
}

// Reputations returns float64 array of reputations
func (s *Simulation) Reputations() []float64 {
	repus := []float64{}
	for _, n := range s.Nodes {
		repus = append(repus, n.Reputation)
	}
	return repus
}

// MaliciousCount returns the number of malicious nodes in the simulation
func (s *Simulation) MaliciousCount() int {
	res := 0
	for _, n := range s.Nodes {
		if n.Malicious {
			res++
		}
	}
	return res
}

// RunCycle executes a full cycle of nrSteps of the simulation
func (s *Simulation) RunCycle(nrSteps int) {
	goodRound, badRound := false, false
	s.RoundsInCycle = nrSteps
	stepCount := 0
	for i := 0; stepCount < nrSteps && s.Running; i++ {
		stepCount++
		s.CycleStep++
		goodRound, badRound, s.Nodes = core.Round(s.Nodes, s.M, s.N, s.RoundNumber, s.CycleSuccesses, s.CommitteeSelectionRule, s.ExternalDistribution)
		if goodRound {
			if s.ConsoleTicks {
				fmt.Printf(".")
			}
			s.Success++
			s.CycleSuccesses++
		} else {
			if s.ConsoleTicks {
				fmt.Printf("o")
			}
		}
		if badRound {
			if s.ConsoleTicks {
				fmt.Printf("x")
			}
			s.BadBlocks++
		}
		if s.CycleStep > nrSteps {
			s.CycleStep = 1
			s.CycleCounter++
			s.CycleSuccessRates = append(s.CycleSuccessRates, s.CycleSuccesses)
			s.CycleSuccesses = 0
		}
		s.RoundNumber++
		// send status update on the update channel every x rounds
		if i%20 == 0 {
			propBuckets := stats.ProportionalBuckets(s.M, s.ProportionalBuckets)
			s.MaliciousNodeRateBuckets = []float64{}
			for i := 0; i < s.ProportionalBuckets; i++ {
				badNode := 0
				for j := propBuckets[i]; j < propBuckets[i+1]; j++ {
					if s.Nodes[j].Malicious {
						badNode++
					}
				}
				s.MaliciousNodeRateBuckets = append(s.MaliciousNodeRateBuckets, float64(badNode)/float64(s.M/s.ProportionalBuckets))
			}
			s.SendStatus(true)
			// if i%s.RoundsInCycle == 0 {
			// 	s.SendStatus(true)
			// } else {
			// 	s.SendStatus(false)
			// }
		}
	}
	// calculate the rate of trusted nodes that were faulty/malicious at round
	faulty90 := 0
	overall90 := 0
	for i := 0; i < s.M/10 && overall90 < s.M; i++ {
		overall90++
		if s.Nodes[i].Malicious {
			faulty90++
		}
	}
	s.TrustedFaultyNodeRate = append(s.TrustedFaultyNodeRate, float64(faulty90)/float64(overall90))
}
