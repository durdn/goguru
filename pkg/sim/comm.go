package sim

// Communication structures for interacting with a Simulation

import (
	"fmt"
	"strconv"
)

// Request holds a Request coming from the client for the Simulation
type Request struct {
	Command     string
	BotsNumber  int
	SybilNumber int
}

// Response holds a Request with updates on the Simulation
type Response struct {
	Command string
	Success bool
	Stats   Stats
}

// Stats holds relevant streamable stats about the simulation
type Stats struct {
	Reputations              []float64
	CycleSuccessRates        []int
	NrNodes                  int
	MaliciousCount           int
	MaliciousNodeRateBuckets []float64
	RoundNumber              int
}

// SendAck sends acknowledgement that message has been received and processed
func (s *Simulation) SendAck(command string, async bool) {
	r := Response{
		Command: command,
		Success: true,
	}
	if async {
		go func() {
			s.ResponseC <- r
		}()
	} else {
		s.ResponseC <- r
	}
}

// SendStatus sends status update on the status channel
func (s *Simulation) SendStatus(sendSuccesses bool) {
	csr := []int{}
	if sendSuccesses {
		limit := len(s.CycleSuccessRates)
		if limit > 1000 {
			csr = s.CycleSuccessRates[limit-1000:]
		} else {
			csr = s.CycleSuccessRates
		}
	}
	go func() {
		s.ResponseC <- Response{
			Command: "stats",
			Success: true,
			Stats: Stats{
				Reputations:              s.Reputations(),
				CycleSuccessRates:        csr,
				MaliciousCount:           s.MaliciousCount(),
				NrNodes:                  len(s.Nodes),
				MaliciousNodeRateBuckets: s.MaliciousNodeRateBuckets,
				RoundNumber:              s.RoundNumber,
			},
		}
	}()
}

// Log logs state of simulation
func (s *Simulation) Log() {
	fmt.Println()
	nr := strconv.FormatFloat(s.TrustedFaultyNodeRate[len(s.TrustedFaultyNodeRate)-1], 'f', 2, 64)
	s.Logger.Log("msg", fmt.Sprintf("Stats after %d cycle(s)", s.CycleCounter))
	s.Logger.Log("msg", "Rate of faulty trusted nodes at round", "cycleCounter", s.CycleCounter, "roundNumber", s.RoundNumber, "TrustedFaultyNodeRate", nr)
	s.Logger.Log("msg", "Nodes ordered by reputation", "repus", fmt.Sprintf("%+v", s.Reputations()[:5]))
	s.Logger.Log("msg", "Successes over 100 rounds", "cyclesuccesses", fmt.Sprintf("%+v", s.CycleSuccessRates))
	s.Logger.Log("msg", "Number of malicious nodes", "malicious", s.MaliciousCount())
	s.Logger.Log("msg", "Rate of trusted nodes that were malicious", "trustedFaultyNodeRate", fmt.Sprintf("%+v", s.TrustedFaultyNodeRate))
	s.Logger.Log("msg", "-")
}
