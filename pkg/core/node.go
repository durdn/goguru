package core

import "fmt"

// Node single Guru Node
type Node struct {
	Reputation float64
	// Is the Node malicious
	Malicious                  bool
	MaxReputation              float64
	MinReputation              float64
	OriginalReputation         float64
	CommitteeMembershipCounter int
}

// Reward increase reputation of node based on the success rate of the cycle
func (n *Node) Reward(sel string, successRate float64) {
	if successRate > 1 {
		fmt.Printf("SuccessRate: %.2f\n", successRate)
		panic("successRate should be a percentage expressed as float below 1")
	}
	if sel == "triang" {
		n.Reputation += (1 - successRate) * ((1 - n.Reputation) / 35)
	} else {
		n.Reputation += (1 - successRate) * ((1 - n.Reputation) / 10)
	}
	if n.Reputation > n.MaxReputation {
		n.MaxReputation = n.Reputation
	}
}

// Penalty decrease reputation of node based on the success rate of the cycle
func (n *Node) Penalty(sel string, successRate float64) {
	if successRate > 1 {
		fmt.Printf("SuccessRate: %.2f\n", successRate)
		panic("successRate should be a percentage expressed as float below 1")
	}
	if sel == "triang" {
		n.Reputation -= successRate * (n.Reputation / 35)
	} else {
		n.Reputation -= successRate * (n.Reputation / 10)
	}
	if n.Reputation < n.MinReputation {
		n.MinReputation = n.Reputation
	}
}

// NewNode instantiates new Guru Node
func NewNode(repu float64, malicious bool) Node {
	return Node{
		Reputation:                 repu,
		Malicious:                  malicious,
		MaxReputation:              repu,
		MinReputation:              repu,
		OriginalReputation:         repu,
		CommitteeMembershipCounter: 0,
	}
}

// ByReputation implements sort.Interface for []Node based on
// the Reputation field.
type ByReputation []Node

func (a ByReputation) Len() int           { return len(a) }
func (a ByReputation) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByReputation) Less(i, j int) bool { return a[i].Reputation > a[j].Reputation }
