package core

import (
	"math"
	"sort"
	"time"

	"golang.org/x/exp/rand"
	"gonum.org/v1/gonum/stat/distuv"

	"bitbucket.org/durdn/goguru/pkg/stats"
)

// selectValidatorsUsing chosen selection rule distribution, nr of nodes, committee size etc.
func selectValidatorsUsing(committeeSelectionRule string, nrNodes int, committeeSize int,
	roundNumber int, externalDistribution string) []int {
	hasExternalDist := externalDistribution != "none"
	res := []int{}
	switch committeeSelectionRule {
	case "exponential":
		res = exponentialCommitteeSelection(nrNodes, committeeSize, roundNumber, hasExternalDist)
	case "triangular":
		res = triangularCommitteeSelection(nrNodes, committeeSize, roundNumber, hasExternalDist)
	case "normal":
	default:
		res = normalCommitteeSelection(nrNodes, committeeSize, roundNumber, hasExternalDist)
	}
	return res
}

// exponentialCommitteeSelection generates random weights using an exponential distribution
func exponentialCommitteeSelection(nrNodes int, committeeSize int, roundNumber int, hasExternalDist bool) []int {
	res := []int{}
	expvar := int((-float64(nrNodes)) / math.Log(0.05))
	if !hasExternalDist {
		expvar = nrNodes
		ctr := 100
		final := (-float64(nrNodes)) / math.Log(0.05)
		for {
			if ctr < roundNumber && float64(expvar) > final {
				expvar = expvar - 500
				ctr += 100
			} else {
				break
			}
		}
		if float64(expvar) < final {
			expvar = int(final)
		}
	}
	if roundNumber >= 0 {
		for i := 0; i < committeeSize; i++ {
			r := rand.New(rand.NewSource(uint64(rand.Int63n(1000000) + time.Now().UnixNano())))
			// Rate is the reciprocal of the scale parameter in the Python version, see
			// https://en.wikipedia.org/wiki/Exponential_distribution
			dist := distuv.Exponential{Rate: 1 / float64(expvar), Src: r}
			candidate := int(math.Abs(dist.Rand()))
			for {
				if candidate >= nrNodes || stats.Contains(res, candidate) {
					r := rand.New(rand.NewSource(uint64(time.Now().UnixNano())))
					dist := distuv.Exponential{Rate: 1 / float64(expvar), Src: r}
					candidate = int(math.Abs(dist.Rand()))
				} else {
					res = append(res, candidate)
					break
				}
			}
		}
	} else {
		panic("WTF: This should be a noop as round number will never be negative")
	}
	return res
}

// triangularCommitteeSelection generates random weights using a triangular distribution
func triangularCommitteeSelection(nrNodes int, committeeSize int, roundNumber int, hasExternalDist bool) []int {
	res := []int{}
	triscale := nrNodes + int(nrNodes/10)
	if !hasExternalDist {
		triscale = nrNodes * 2
		ctr := 100
		final := nrNodes
		for {
			if ctr < roundNumber && triscale > final {
				triscale = triscale - 1000
				ctr += 100
			} else {
				break
			}
		}
		if triscale <= nrNodes {
			triscale = nrNodes + int(nrNodes/5)
		}
	}
	if roundNumber >= 0 {
		for i := 0; i < committeeSize; i++ {
			r := rand.New(rand.NewSource(uint64(rand.Int63n(1000000) + time.Now().UnixNano())))
			loc := 0.0
			scale := float64(triscale)
			dist := distuv.NewTriangle(loc, loc+scale, 0)
			dist.Src = r
			candidate := int(math.Abs(dist.Rand()))
			for {
				if candidate >= nrNodes || stats.Contains(res, candidate) {
					r := rand.New(rand.NewSource(uint64(time.Now().UnixNano())))
					// Something strange about the parameter for the triangular distribution in Python:
					//   triang.rvs(c=0,loc=0,scale=triscale)
					//   c parameter 0 makes it strange to me looking at:
					//   https://en.wikipedia.org/wiki/Triangular_distribution
					//   https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.triang.html
					dist := distuv.NewTriangle(loc, loc+scale, 0)
					dist.Src = r
					candidate = int(math.Abs(dist.Rand()))
				} else {
					res = append(res, candidate)
					break
				}
			}
		}
	} else {
		panic("WTF: This should be a noop as round number will never be negative")
	}
	return res
}

// normalCommitteeSelection generates random weights using a normal distribution
// https://en.wikipedia.org/wiki/Generalized_normal_distribution#Version_1
// TODO: implement
func normalCommitteeSelection(nrNodes int, committeeSize int, roundNumber int, hasExternalDist bool) []int {
	res := []int{}
	if roundNumber >= 0 {
	} else {
		panic("WTF: This should be a noop as round number will never be negative")
	}
	return res
}

// Round performs a committee selection and validation round
// Returns if the round was good or bad (isRoundgGood),
// if the round was so bad that it created a bad block (isBlockBad),
// and the updated list of nodes
func Round(nodes []Node, nrNodes int, committeeSize int, roundNumber int, cycleSuccesses int,
	committeeSelectionRule string, externalDistribution string) (isRoundGood bool, isBlockBad bool, res []Node) {
	res = nodes[:]
	correctValidator := 0
	threshold := float64(2*committeeSize) / 3
	badthreshold := float64(committeeSize) / 3
	successRate := float64(cycleSuccesses) / 100
	validators := selectValidatorsUsing(committeeSelectionRule, nrNodes, committeeSize, roundNumber, externalDistribution)
	for i := 0; i < committeeSize; i++ {
		if !res[validators[i]].Malicious {
			correctValidator++
			res[validators[i]].CommitteeMembershipCounter++
		}
	}
	for i := 0; i < committeeSize; i++ {
		if float64(correctValidator) > threshold || float64(correctValidator) < badthreshold {
			res[validators[i]].Reward(committeeSelectionRule, successRate)
		} else {
			res[validators[i]].Penalty(committeeSelectionRule, successRate)
		}
	}
	sort.Sort(ByReputation(res))
	isRoundGood = float64(correctValidator) > threshold
	isBlockBad = float64(correctValidator) < badthreshold
	return isRoundGood, isBlockBad, res
}
