package core

import (
	"math/rand"
	"testing"
	"time"

	"bitbucket.org/durdn/goguru/pkg/stats"
)

// func TestSelectValidators(t *testing.T) {
// 	res := SelectValidatorsUsing("triangular", 20, 10, 0, "normal")
// 	if len(res) != 10 {
// 		t.Errorf("committee selection should be 10 elements: %+v", res)
// 	}
// }

func TestExponentialCommitteeSelection(t *testing.T) {
	res := exponentialCommitteeSelection(20, 10, 0, false)
	if len(res) != 10 {
		t.Errorf("committee selection should be 10 elements: %+v", res)
	}
}

func TestTriangularCommitteeSelection(t *testing.T) {
	res := triangularCommitteeSelection(20, 10, 0, false)
	if len(res) != 10 {
		t.Errorf("committee selection should be 10 elements: %+v", res)
	}
}

func TestSelectValidatorsUsing(t *testing.T) {
	res := selectValidatorsUsing("exponential", 20, 10, 0, "none")
	if len(res) != 10 {
		t.Errorf("committee selection should be 10 elements: %+v", res)
	}
}

func TestRound(t *testing.T) {
	nodes := []Node{}
	alpha0 := 0.4
	for i := 0; i < 20; i++ {
		time.Sleep(10 * time.Millisecond)
		randomInitialReputation := stats.InitialReputation("exponential")
		malicious := rand.Float64() < ((alpha0-0.05)*(1-randomInitialReputation) + 0.05)
		node := NewNode(randomInitialReputation, malicious)
		nodes = append(nodes, node)
	}
	b1, b2, res := Round(nodes, 20, 10, 0, 0.0, "exponential", "none")
	if len(res) != 20 {
		t.Errorf("Result after one round: %v %v", b1, b2)
		// b, err := json.MarshalIndent(res, "", "  ")
		// if err != nil {
		// 	fmt.Println("error:", err)
		// }
		// fmt.Print(string(b))
	}
}
