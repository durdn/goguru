package daemon

import (
	"errors"
	"sync"

	"bitbucket.org/durdn/goguru/pkg/sim"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{} // use default options

// Client lightweight websocket client connection, with read/write mutexes
type Client struct {
	Conn       *websocket.Conn
	rmu        sync.Mutex
	wmu        sync.Mutex
	Simulation sim.Simulation
}

func (c *Client) read() ([]byte, error) {
	if c != nil {
		c.rmu.Lock()
		_, bytes, err := c.Conn.ReadMessage()
		c.rmu.Unlock()
		return bytes, err
	}
	return nil, errors.New("tried to read from client")
}

func (c *Client) write(bytes []byte) error {
	if c != nil {
		c.wmu.Lock()
		err := c.Conn.WriteMessage(websocket.TextMessage, bytes)
		c.wmu.Unlock()
		return err
	}
	return errors.New("tried to write to nil client")
}
