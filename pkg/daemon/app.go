package daemon

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/durdn/goguru/pkg/sim"
	"github.com/go-kit/kit/log"
)

// App context holds global initialisation
type App struct {
	Addr        string
	Logger      log.Logger
	Clients     map[string]*Client
	AssetsPath  string
	SSLCertPath string
	SSLKeyPath  string
}

func (a *App) processRequestLoop(remoteAddr string) {
	msg := sim.Request{Command: ""}
	for {
		bytes, err := a.Clients[remoteAddr].read()
		if err != nil {
			a.Logger.Log("read", err, "ip", remoteAddr)
			a.Clients[remoteAddr].Conn.Close()
			delete(a.Clients, remoteAddr)
			break
		}
		err = json.Unmarshal(bytes, &msg)
		if err != nil {
			a.Logger.Log("unmarshal", err, "ip", remoteAddr)
			a.Clients[remoteAddr].Conn.Close()
			delete(a.Clients, remoteAddr)
			break
		}
		a.Logger.Log("recv", msg.Command, "ip", remoteAddr)
		a.Clients[remoteAddr].Simulation.RequestC <- msg
	}
}

func (a *App) processResponseLoop(remoteAddr string) {
	for {
		if _, ok := a.Clients[remoteAddr]; ok {
			select {
			case u := <-a.Clients[remoteAddr].Simulation.ResponseC:
				updateJSON, err := json.Marshal(u)
				if err != nil {
					a.Logger.Log("marshal", err, "ip", remoteAddr)
					break
				}
				err = a.Clients[remoteAddr].write(updateJSON)
				if err != nil {
					a.Logger.Log("write error", err, "ip", remoteAddr)
					break
				}
				if u.Command != "stats" {
					a.Logger.Log("sent", u.Command, "ip", remoteAddr)
				}
			}
		} else {
			break
		}
	}
}

// Websocket creates a web socket connection with a client
//   starts up a simulation per client
//   enters a loop receiving requests from the client and sending
//   simulation responses to it
func (a *App) Websocket(w http.ResponseWriter, r *http.Request) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	var err error

	if _, ok := a.Clients[r.RemoteAddr]; !ok {
		c := Client{}
		c.Conn, err = upgrader.Upgrade(w, r, nil)
		if err != nil {
			a.Logger.Log("upgrade", err, "ip", r.RemoteAddr)
			return
		}
		a.Logger.Log("msg", "new client connected", "ip", r.RemoteAddr)
		a.Clients[r.RemoteAddr] = &c
		defer c.Conn.Close()

		a.Clients[r.RemoteAddr].Simulation = sim.NewSimulation(1000, 20, "none", 0.4, "exponential", a.Logger)
		a.Logger.Log("msg", "Nodes ordered by initial reputation", "repus", fmt.Sprintf("%+v", a.Clients[r.RemoteAddr].Simulation.Reputations()[:5]))
		a.Logger.Log("msg", "Malicious nodes", "malicious", a.Clients[r.RemoteAddr].Simulation.MaliciousCount(), "total", len(a.Clients[r.RemoteAddr].Simulation.Nodes))
		a.Logger.Log("msg", "Simulation initialised", "ip", r.RemoteAddr)

		// Start simulation loop in a go routine
		go func() {
			a.simulationLoop(r.RemoteAddr)
		}()
		go func() {
			a.processRequestLoop(r.RemoteAddr)
		}()
		a.processResponseLoop(r.RemoteAddr)
	} else {
		a.Logger.Log("msg", "backend already knows this client, reconnect", "ip", r.RemoteAddr)
	}
}

func (a *App) simulationLoop(remoteAddr string) {
	for {
		if _, ok := a.Clients[remoteAddr]; ok {
			a.Clients[remoteAddr].Simulation.Loop()
		} else {
			a.Logger.Log("msg", "client does not exist anymore, cannot start loop", "ip", remoteAddr)
			return
		}
	}
}
