package stats

import (
	"time"

	"golang.org/x/exp/rand"
	"gonum.org/v1/gonum/stat/distuv"
)

// Contains checks if number appears in array. Some basics are missing in Go
func Contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// ProportionalBuckets generate indexes to divide and array in proportional equal parts
func ProportionalBuckets(nr int, eachPart int) (res []int) {
	for i := 0; i <= eachPart; i++ {
		res = append(res, i*nr/eachPart)
	}
	return res
}

// InitialReputation calculate a random initial reputation given a probability distribution
func InitialReputation(distribution string) float64 {
	temp := float64(0)
	if distribution == "none" {
		return 0.0
	}
	if distribution == "uniform" {
		r := rand.New(rand.NewSource(uint64(rand.Int63n(1000000) + time.Now().UnixNano())))
		dist := distuv.Uniform{Min: 0.0, Max: 0.9999, Src: r}
		temp = dist.Rand()
	}
	if distribution == "normal" {
		r := rand.New(rand.NewSource(uint64(rand.Int63n(1000000) + time.Now().UnixNano())))
		dist := distuv.Normal{Mu: 0.5, Sigma: 0.15, Src: r}
		temp = dist.Rand()
		for {
			if temp < 0.0 || 1.0 < temp {
				dist := distuv.Normal{Mu: 0.5, Sigma: 0.5}
				temp = dist.Rand()
			} else {
				break
			}
		}
		return temp
	}
	if distribution == "exponential" {
		r := rand.New(rand.NewSource(uint64(time.Now().UnixNano())))
		dist := distuv.Exponential{Rate: 1 / 0.3, Src: r}
		temp = dist.Rand()
		for {
			if temp < 0 || 1 < temp {
				dist := distuv.Exponential{Rate: 1 / 0.3, Src: r}
				temp = dist.Rand()
			} else {
				break
			}
		}
		return temp
	}
	return temp
}
