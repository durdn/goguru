package stats

import "testing"

func TestContains(t *testing.T) {
	res := Contains([]int{1, 2, 3, 10, 7}, 10)
	if !res {
		t.Errorf("contain function is this array contains 10")
	}
}

func TestProportionalBuckets(t *testing.T) {
	res := ProportionalBuckets(5000, 10)
	if len(res) != 11 {
		t.Errorf("Proportional sets should contain 11 markers: %+v", res)
	}
	if res[1] != 500 {
		t.Errorf("Proportional set index should be 500 for the second proportional set: %+v", res)
	}
}
